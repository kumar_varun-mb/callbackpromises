const fs = require('fs');
const path = require('path');
const promise = require('../problem2.js');

promise.readFile('./data/lipsum.txt')
.then((data) => {
    return promise.writeFile('upperCase.txt', 'data', data.toUpperCase());
})
.then((fileName) => {
    return promise.storeFileName(fileName);
})
.then((fileName) => {
    return promise.readFile(`./data/${fileName}`);
})
.then((data) => {
    return promise.splitLowerCaseData(data)
})
.then((splitData) => {
    return promise.writeFile('splitLipsum.txt', 'data', splitData);
})
.then((fileName) => {
    return promise.storeFileName(fileName); 
})
.then((fileName) => {
    return promise.readFile(`./data/${fileName}`);
})
.then((data) => {
    return promise.splitSort(data);
})
.then((splitSortData) => {
    return promise.writeFile('splitSort.txt', 'data', splitSortData);
})
.then((fileName) => {
    promise.storeFileName(fileName); 
})
.then(() => {
    return promise.readFile('./data/fileNames.txt')
})
.then((data) => {
   promise.deleteFiles(data);
});
