const json = require('../problem1.js');

let directoryName = 'JSON';
let jsonFileName = 'file.json';
let data = {
    name: 'varun',
    age: 25
};

async function testProblem1Async() {
    try {
        const createDirectory = await json.createDirectory(directoryName, jsonFileName, data);
        console.log(createDirectory.message);
        const createJsonFile = await json.createJsonFile(createDirectory.jsonFilePath, createDirectory.data);
        console.log(createJsonFile.message);
        const deleteJsonFile = await json.deleteJsonFile(createJsonFile.jsonFilePath);
        console.log(deleteJsonFile.message);
    }
    catch (err) {
        console.log(err);
    }
}
testProblem1Async();