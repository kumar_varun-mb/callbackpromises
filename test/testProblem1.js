const json = require('../problem1.js');

let directoryName = 'JSON';
let jsonFileName = 'file.json';
let data = {
    name: 'varun',
    age: 25
};

json.createDirectory(directoryName, jsonFileName, data)
.then((obj) => {
    console.log(obj.message);
    return json.createJsonFile(obj.jsonFilePath, obj.data);
})
.then((obj) => {
    console.log(obj.message);
    return json.deleteJsonFile(obj.jsonFilePath);
})
.then((obj) => {
    console.log(obj.message);
})
.catch((err) => {
    console.log(err);
})