const fs = require('fs');
const path = require('path');
const promise = require('../problem2.js');

async function testProblem2Async() {
    try {
        const lipsumData = await promise.readFile('./data/lipsum.txt');
        const upperCaseLipsum = await promise.writeFile('upperCase.txt', 'data', lipsumData.toUpperCase());
        await promise.storeFileName(upperCaseLipsum);
        const upperCaseLipsumData = await promise.readFile(`./data/${upperCaseLipsum}`);
        const splitLipsumData = await promise.splitLowerCaseData(upperCaseLipsumData);
        const splitLipsum = await promise.writeFile('splitLipsum.txt', 'data', splitLipsumData);
        await promise.storeFileName(splitLipsum);
        const splitSortLipsumData = await promise.splitSort(splitLipsumData);
        const splitSortLipsum = await promise.writeFile('splitSort.txt', 'data', splitSortLipsumData);
        await promise.storeFileName(splitSortLipsum);
        const fileNames = await promise.readFile('./data/fileNames.txt');
        await promise.deleteFiles(fileNames);
    }
    catch(err){
        console.log(err);
    }

}
testProblem2Async();