/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

const fs = require('fs');
const { resolve } = require('path');
const path = require('path');

function readFile(filePath){
    return new Promise((resolve, reject) => {
        fs.readFile(filePath, 'utf-8', (err, data) => {
            if(err)
                reject(err);
            else
                resolve(data);
        });
    });
}

function writeFile(fileName, directoryName, data){
    let filePath = path.join(__dirname, directoryName, fileName);
    return new Promise((resolve, reject) => {
        fs.writeFile(filePath, data, 'utf-8', 
        (err)=> {
            if(err) 
                reject(err);
            else
                resolve(fileName);
        })
    });
}

function storeFileName(data){
    return new Promise((resolve, reject) => {
        fs.appendFile(path.join(__dirname,'data/fileNames.txt'), data+'\n', (err) => {
            if(err)
                reject(err);
            else
                // console.log(data+' saved in '+ path.join(__dirname,'data/fileNames.txt'));
                resolve(data);
        });
    });
    
}

const splitLowerCaseData = (data) => {
    return new Promise((resolve, reject) => {
        resolve(data.toLowerCase().split('.').map((str)=> {
            return str.trim();
        }).join('\n'));
    })
}

const splitSort = (data) => {
    return new Promise((resolve, reject) => {
        resolve(data.split('\n').sort().join('\n'));
    })
}

const deleteFiles = (data) => {
    return new Promise((resolve, reject) => {
        fileNames = data.split('\n'); 
        fileNames.forEach((file) => { 
            if(file){
            fs.unlink(path.join(__dirname, `./data/${file}`), 
            (err) => { 
                if(err) 
                    console.log('Delete error'); 
                else 
                    console.log(`Deleted ${file}`); } )
            } 
        })
    })
}



module.exports = {readFile, writeFile, storeFileName, splitLowerCaseData, splitSort, deleteFiles};