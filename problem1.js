/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/

const fs = require('fs');
const path = require('path');

function createDirectory(directoryName, jsonFileName, data) {
    return new Promise((resolve, reject) => {
        fs.mkdir(path.join(__dirname, directoryName), (err) => {
            if (err)
                reject('Could not create Directory. Error: \n', err);
            else {
                let obj = {
                    message: 'Directory Created',
                    jsonFilePath: path.join(__dirname, directoryName, jsonFileName),
                    data: data
                };
                resolve(obj);
            }
        });
    });
}

function createJsonFile(jsonFilePath, data) {
    return new Promise((resolve, reject) => {
        fs.writeFile(jsonFilePath, data, 'utf-8', (err) => {
            if (err)
                reject('Could not create JSON file. Error: \n', err);
            else {
                let obj = {
                    message: 'JSON File Created',
                    jsonFilePath: jsonFilePath,
                };
                resolve(obj);
            }
        });
    });
}

function deleteJsonFile(directory) {
    let obj = {
        message: ''
    };
    return new Promise((resolve, reject) => {
        fs.unlink(directory, (err) => {
            if (err) {
                obj.message = 'Could not delete file: \n', err;
                reject(obj);
            }
            else {
                obj.message = 'JSON File Deleted.';
                resolve(obj);
            }
        });
    });
}

module.exports = { createDirectory, createJsonFile, deleteJsonFile };